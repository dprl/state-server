FROM python:3.8-buster

COPY ./ ./
RUN pip install -r requirements.txt

EXPOSE 8081

CMD ["python", "serve-state.py"]

